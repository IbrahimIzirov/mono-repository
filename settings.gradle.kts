rootProject.name = "mono"

/**
 *  Include 'tools' folder as a build directory, it contains all required build scripts and plugins
 */
includeBuild("tools")

file("projects").listFiles()?.forEach { p: File ->
  if (p.isDirectory()) {
    include(p.name)
    val project = project(":${p.name}")
    project.projectDir = p
  }
}

///**
// * Configuration for Redis-Gradle build cache system.
// */
//
//buildscript {
//  repositories {
//    maven {
//      url = uri("https://plugins.gradle.org/m2")
//    }
//  }
//
//  dependencies {
//    classpath("net.idlestate:gradle-redis-build-cache:1.3.0")
//  }
//}
//
//plugins {
//  id("net.idlestate.gradle-redis-build-cache") version "1.3.0"
//}
//
//buildCache {
//  local {
//    isEnabled = false
//  }
//  registerBuildCacheService(
//    RedisBuildCache::class.java,
//    RedisBuildCacheServiceFactory::class.java
//  )
//
//  remote(RedisBuildCache::class.java) {
//    host = "localhost"
//    port = 6379
//    password = "eyJskaKDasdkDAWkadAk2141"
//    isEnabled = true
//    isPush = true
//    timeToLive = 2 * 24 * 60 // Two days in minutes
//  }
//}
