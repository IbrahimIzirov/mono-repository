plugins {
    id("org.sonarqube")
}

sonarqube {
    properties {
        property("sonar.login", System.getenv("SQ_TOKEN"))
        property("sonar.host.url", System.getenv("SQ_HOST"))
    }
}

subprojects {
    sonarqube {
        properties {
            property("sonar.version", System.getenv("CI_COMMIT_SHORT_SHA"))
            property("sonar.projectKey", System.getenv("CI_PROJECT_NAME"))
            property("sonar.sources", "src/main/java")
            property("sonar.tests", "src/**/java")
            property("sonar.jacoco.reportPaths", "build/reports/jacoco/test/jacocoTestReport.xml")
        }
    }
}