plugins {
  id("io.spring.dependency-management")
  id("java-common-conventions")
  id("idea")
  id("sonarqube-conventions")
  jacoco
}

repositories {
  mavenCentral()
  maven { url = uri("https://repo.spring.io/milestone") }
}

extra["springBootVersion"] = "3.1.2"
extra["springCloudVersion"] = "2022.0.3"
extra["testcontainersVersion"] = "1.18.3"

dependencyManagement {
  imports {
    mavenBom("org.springframework.boot:spring-boot-dependencies:${property("springBootVersion")}")
    mavenBom("org.testcontainers:testcontainers-bom:${property("testcontainersVersion")}")
    mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
  }
}

configurations {
  compileOnly {
    extendsFrom(configurations.annotationProcessor.get())
  }
}

dependencies {
  implementation("jakarta.validation:jakarta.validation-api")
  implementation("jakarta.annotation:jakarta.annotation-api")
  implementation("org.springframework.boot:spring-boot-devtools")
  compileOnly("org.projectlombok:lombok:1.18.30")
  annotationProcessor("org.projectlombok:lombok:1.18.30")
  annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")

  constraints {
    // Define dependency versions as constraints
    implementation("io.minio:minio:8.5.4")
    // If you want to have ESC compliant logs, you should add this dependency.
    implementation("co.elastic.logging:logback-ecs-encoder:1.6.0")
    implementation("io.hypersistence:hypersistence-utils-hibernate-63:3.8.2")
    runtimeOnly("com.oracle.database.jdbc:ojdbc10:19.19.0.0")
    testImplementation("com.squareup.okhttp3:mockwebserver:4.12.0")
    testImplementation("org.wiremock:wiremock-standalone:3.9.1")
    testRuntimeOnly("com.squareup.okhttp3:okhttp:4.12.0")
  }
}

sourceSets {
  create("integrationTest") {
    compileClasspath += sourceSets.main.get().output
    runtimeClasspath += sourceSets.main.get().output
  }
}

idea {
  module {
    testSources.from(sourceSets["integrationTest"].allJava.srcDirs)
    testResources.from(sourceSets["integrationTest"].resources.srcDirs)
  }
}

val integrationTestImplementation: Configuration by configurations.getting {
  extendsFrom(configurations.testImplementation.get())
}

configurations["integrationTestRuntimeOnly"].extendsFrom(configurations.testRuntimeOnly.get())

val integrationTest = task<Test>("integrationTest") {
  description = "Runs integration tests."
  group = "verification"

  testClassesDirs = sourceSets["integrationTest"].output.classesDirs
  classpath = sourceSets["integrationTest"].runtimeClasspath
}

tasks.check {
  dependsOn(tasks.test, integrationTest)
}

tasks.jacocoTestReport {
  reports {
    xml.required.set(true)
    csv.required.set(false)
  }
  dependsOn(tasks.test, integrationTest)
  executionData(fileTree(buildDir).include("/jacoco/*.exec"))
  classDirectories.setFrom(
    files(classDirectories.files.map {
      fileTree(it) {
        exclude("**/restapi/**")
      }
    })
  )
}
