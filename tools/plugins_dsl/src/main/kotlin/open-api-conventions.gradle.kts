import org.openapitools.generator.gradle.plugin.tasks.GenerateTask
import org.openapitools.generator.gradle.plugin.tasks.ValidateTask

plugins {
    id("java-common-conventions")
    id("org.openapi.generator")
}

val inputSpecPath = "$projectDir/src/main/resources/service-rest-api.yaml"
val outputSpecPath = "$projectDir/generated-api"
val groupStr = "com.mono"

dependencies {
    implementation("jakarta.validation:jakarta.validation-api")
    implementation("io.swagger.core.v3:swagger-annotations:2.2.15")
    implementation("org.webjars:swagger-ui:5.1.3")
    implementation("org.openapitools:jackson-databind-nullable:0.2.6")
}

sourceSets {
    main {
        java {
            srcDir("${outputSpecPath}/src/main/java")
        }
    }
}

tasks.withType<GenerateTask> {
    inputSpec.set(inputSpecPath)
    outputDir.set(outputSpecPath)
    validateSpec.set(true)
    generatorName.set("spring")
    apiPackage.set("${groupStr}.restapi.api")
    invokerPackage.set("${groupStr}.restapi.invoker")
    modelPackage.set("${groupStr}.restapi.model")
    configOptions.set(
            mapOf(
                    "library" to "spring-boot",
                    "useSpringBoot3" to "true",
                    "interfaceOnly" to "true",
                    "serializable Model" to "true",
                    "singleContentTypes" to "true",
//                    "skipDefaultInterface" to "true",
                    "reactive" to "true",
                    "useOptional" to "true",
                    "useBeanValidation" to "true",
            )
    )

    outputs.upToDateWhen { true }
    outputs.cacheIf { true }

    doLast {
        delete("${outputSpecPath}/.openapi-generator")
        delete("${outputSpecPath}/.openapi-generator-ignore")
        delete("${outputSpecPath}/README.md")
        delete("${outputSpecPath}/pom.xml")
    }
}

tasks.withType<ValidateTask> {
    inputSpec.set(inputSpecPath)
    recommend.set(true)

    outputs.upToDateWhen { true }
    outputs.cacheIf { true }
}

tasks.getByName("clean") {
    delete(outputSpecPath)
}

tasks.getByName("compileJava") {
    dependsOn("openApiGenerate")
}

tasks.getByName("check") {
    dependsOn("openApiValidate")
}
