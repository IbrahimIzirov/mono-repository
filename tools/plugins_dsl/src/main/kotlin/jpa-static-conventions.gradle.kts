// This plugin is used when we need to be type safety in writing Criteria queries.
// for example it's not right to use operation.get("channelId").
// Instead when you add this plugin where it's needed it will generate folder with static Entities.
// and then use operation.get(channelId).

plugins {
  id("java-common-conventions")
}

val outputSpecPath = "$projectDir/generated-jpa-sources"

dependencies {
  implementation("jakarta.persistence:jakarta.persistence-api:3.1.0")

  // Version of jpamodelgen should be the same one as version of hibernate-core.
  annotationProcessor("org.hibernate.orm:hibernate-jpamodelgen:6.2.6.Final")
}

sourceSets {
  main {
    java {
      srcDir(outputSpecPath)
    }
  }
}

tasks.compileJava {
  options.generatedSourceOutputDirectory.set(file(outputSpecPath))
}

tasks.withType<JacocoReport> {
  classDirectories.setFrom(
    files(classDirectories.files.map {
      fileTree(it) {
        exclude("**/generated/**")
      }
    })
  )
}

tasks.getByName<Delete>("clean") {
  delete(outputSpecPath)
}
