import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
  java
}

java {
  sourceCompatibility = JavaVersion.VERSION_17
  targetCompatibility = JavaVersion.VERSION_17
}

//val nexusUser =
//  if (System.getenv("NEXUS_REGISTRY_USER") != null) System.getenv("NEXUS_REGISTRY_USER")
//  else providers.gradleProperty("nexusUser").get()
//
//val nexusPassword =
//  if (System.getenv("NEXUS_REGISTRY_PASSWORD") != null) System.getenv("NEXUS_REGISTRY_PASSWORD")
//  else providers.gradleProperty("nexusPassword").get()

repositories {
  mavenCentral()
  mavenLocal()
//  maven {
//    url = uri("https://nexus.com/repositories/maven-public")
//    credentials {
//      username = nexusUser
//      password = nexusPassword
//    }
//  }
}

dependencies {
  testRuntimeOnly("org.junit.platform:junit-platform-launcher")

  constraints {
    // Define dependency versions as contraints
    implementation("jakarta.validation:jakarta.validation-api:3.0.2")
    implementation("jakarta.annotation:jakarta.annotation-ap1:2.1.1")
    testImplementation("org.assertj:assertj-core:3.24.2")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.3")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.3")
  }
}

tasks.withType<Test> {
  useJUnitPlatform()

  testLogging {
    showExceptions = true
    showStandardStreams = true
    events(TestLogEvent.PASSED, TestLogEvent.SKIPPED, TestLogEvent.FAILED)
  }
}
