plugins {
  id("org.springframework.boot")
  id("spring-boot-dependencies")
  id("java-common-conventions")
  id("idea")
}

tasks.named<org.springframework.boot.gradle.tasks.bundling.BootBuildImage>("bootBuildImage") {
  imageName.set("internal-image-repository-${project.name}")
  publish.set(true)
  docker {
    publishRegistry {
      username.set(System.getenv("CI_REGISTRY_USER"))
      password.set(System.getenv("CI_REGISTRY_PASSWORD"))
    }
  }
  environment.set(
    mapOf(
      "BP_SPRING_CLOUD_BINDINGS_DISABLED" to "true",
      "HTTP_PROXY" to "proxy_company:proxy_port",
      "HTTPS_PROXY" to "proxy_company:proxy_port"
    )
  )
}

springBoot {
  buildInfo()
}

tasks.bootJar {
  duplicatesStrategy = (DuplicatesStrategy.EXCLUDE)
}
