import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	`kotlin-dsl`
}

repositories {
	gradlePluginPortal()
}

val javaVersion = JavaVersion.VERSION_17

tasks.withType<KotlinCompile> {
	kotlinOptions {
		jvmTarget = javaVersion.toString()
	}
}

java {
	sourceCompatibility = javaVersion
	targetCompatibility = javaVersion
}

dependencies {
	implementation("org.springframework.boot:spring-boot-gradle-plugin:3.1.2")
	implementation("io.spring.gradle:dependency-management-plugin:1.1.2")
	implementation("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:4.3.0.3225")
	implementation("org.openapitools:openapi-generator-gradle-plugin:6.6.0")
}

fun plugin (id: String, version: String) = "$id:$id.gradle.plugin:$version"
