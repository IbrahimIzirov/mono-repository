package com.local.monitoring;

import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.function.Supplier;

class MetricsNotificationQueueConfiguration {

  private final Sinks.Many<MetricsNotification> bufferCat = Sinks.many().multicast().onBackpressureBuffer();

  @Bean
  MetricsNotificationRequestStreamService metricsNotificationRequestStreamService() {
    return bufferCat::tryEmitNext;
  }

  @Bean("metrics-notification")
  Supplier<Flux<MetricsNotification>> qMetricsNotificationDeclaration() {
    return bufferCat::asFlux;
  }
}
