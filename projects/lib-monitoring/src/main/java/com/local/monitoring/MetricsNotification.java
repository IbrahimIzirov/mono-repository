package com.local.monitoring;

import java.util.Map;

public record MetricsNotification(
  int periodId,
  int deliveryId,
  String source,
  Map<String, Object> properties
) {
}
