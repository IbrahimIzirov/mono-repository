package com.local.monitoring;

public interface MetricsNotificationRequestStreamService {
  void apply(MetricsNotification metricsNotification);
}
