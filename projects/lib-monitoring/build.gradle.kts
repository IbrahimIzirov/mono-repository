plugins {
  id("spring-boot-conventions")
}

group = "com.local"

dependencies {
  compileOnly("org.springframework.boot:spring-boot-starter-webflux")
}
