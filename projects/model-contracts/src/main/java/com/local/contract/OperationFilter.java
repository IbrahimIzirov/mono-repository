package com.local.contract;

import com.local.contract.page.Paging;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import jakarta.annotation.Nullable;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OperationFilter extends Paging {
  private int companyId;
  private int turnover;
  private String period;
  private @Nullable OperationType type;
  private @Nullable Collection<JobStatus> jobStatuses;
  private @Nullable String search;
  private @Nullable OperationType sort;
}
