package com.local.contract.page;

import com.local.contract.Operation;

import java.util.Collection;

public record PageResponse(
  long totalElements,
  int totalPages,
  Collection<Operation> content
) {
}
