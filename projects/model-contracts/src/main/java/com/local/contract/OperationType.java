package com.local.contract;

public enum OperationType {
  SALES_VOLUME,
  PRICE,
  TURNOVER
}
