package com.local.contract;

public enum OutlierType {
  SALES_VOLUME,
  PRICE,
  TURNOVER
}
