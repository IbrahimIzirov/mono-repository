package com.local.contract;

import java.util.stream.Stream;

public enum JobStatus {

  NONE(0),
  NO_ERROR(1),
  RELEASED(2),
  URGENT_CASE(4),
  CODE_MATCH(8),
  TEST_LINE(16),
  PRODUCTION_LINE(32),
  OTHER(256);

  private final int numberRepresentation;

  JobStatus(final int numberRepresentation) {
    this.numberRepresentation = numberRepresentation;
  }

  public static JobStatus of(int representation) {
    return Stream.of(JobStatus.values())
      .filter(js -> js.numberRepresentation == representation)
      .findFirst()
      .orElseThrow();
  }

  public int getNumberRepresentation() {
    return this.numberRepresentation;
  }
}
