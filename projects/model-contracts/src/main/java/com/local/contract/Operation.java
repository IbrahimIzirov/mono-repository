package com.local.contract;

import jakarta.annotation.Nullable;

public record Operation(
  long shopId,
  double priceOrg,
  double turnover,
  long objectId,
  String dashboard,
  String brand,
  String channel,
  @Nullable
  Integer juwt
) {
}
