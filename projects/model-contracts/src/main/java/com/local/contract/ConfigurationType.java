package com.local.contract;

import java.util.Map;
import java.util.stream.Stream;

public enum ConfigurationType {

  NORMAL(Map.of("KNEE", "NORMAL")),
  BINORMAL(Map.of("KNEE", "BINORMAL"));

  private final Map<String, Object> configValue;

  ConfigurationType(final Map<String, Object> configValue) {
    this.configValue = configValue;
  }

  public static ConfigurationType of(Map<String, Object> configValue) {
    return Stream.of(ConfigurationType.values())
      .filter(cv -> cv.configValue.equals(configValue))
      .findFirst()
      .orElseThrow();
  }
}
