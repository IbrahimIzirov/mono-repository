package com.local.contract.page;

import lombok.Data;

@Data
public abstract class Paging {
  protected int pageNumber = 1;
  protected int pageSize = 10;
  protected Direction order = Direction.DESC;
}
