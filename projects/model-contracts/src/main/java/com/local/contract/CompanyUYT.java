package com.local.contract;

import jakarta.annotation.Nullable;

public record CompanyUYT(
  long companyId,
  int countryId,
  long periodId,
  JobStage jobStage
) {

  public record JobStage(
    @Nullable
    String producer,
    String timeLine,
    JobStatus status
  ) {
  }
}
