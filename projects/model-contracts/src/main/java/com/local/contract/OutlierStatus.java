package com.local.contract;

public enum OutlierStatus {
  CLASSIFIED,
  TO_BE_CONFIRMED,
  CONFIRMED,
  INCORRECT,
  OTHERS,
  NONE
}
