package com.local.contract;

public enum Periodicity {
  WEEKLY, MONTHLY
}
