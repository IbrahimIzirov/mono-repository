package com.local.contract;

public record Company(
  int id,
  String name
) {
}
