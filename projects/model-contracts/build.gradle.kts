plugins {
  id("spring-boot-dependencies")
}

group = "com.local"

dependencies {
  compileOnly("org.projectlombok:lombok")
  compileOnly("com.fasterxml.jackson.core:jackson-databind")
  annotationProcessor("org.projectlombok:lombok")
  testCompileOnly("org.projectlombok:lombok")
}
