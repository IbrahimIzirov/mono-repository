
plugins {
  id("spring-boot-conventions")
  id("jpa-static-conventions")
}

group = "com.local"

dependencies {
  implementation(project(":lib-metrics"))
  implementation(project(":lib-monitoring"))
  implementation(project(":model-contracts"))
  implementation(project(":utilities"))
  implementation("org.springframework.boot:spring-boot-starter-actuator")
  implementation("org.springframework.boot:spring-boot-starter-validation")
  implementation("org.springframework.boot:spring-boot-starter-data-jpa")
  implementation("org.springframework.boot:spring-boot-starter-webflux")
  implementation("org.springframework.cloud:spring-cloud-stream-binder-rabbit")
  implementation("io.hypersistence:hypersistence-utils-hibernate-63")
  // If you have to produce ESC compliant logs from logback file.
  implementation("co.elastic.logging:logback-ecs-encoder")
  implementation("org.flywaydb:flyway-core")
  runtimeOnly("org.postgresql:postgresql")
  runtimeOnly("io.micrometer:micrometer-registry-prometheus")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("org.assertj:assertj-core")
  testImplementation("org.testcontainers:junit-jupiter")
  testImplementation("org.testcontainers:postgresql")
  testImplementation("org.testcontainers:rabbitmq")
}
