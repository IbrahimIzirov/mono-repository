package com.local;

import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
public interface RabbitMQContainer {

  @Container
  org.testcontainers.containers.RabbitMQContainer rabbitMQ = new org.testcontainers.containers.RabbitMQContainer(
    DockerImageName.parse("rabbitmq:3")
      .asCompatibleSubstituteFor("rabbitmq"))
    .withVhost("mono")
    .withExposedPorts(5672, 15672);

  @DynamicPropertySource
  static void setProperties(DynamicPropertyRegistry registry) {
    registry.add("spring.rabbitmq.host", rabbitMQ::getHost);
    registry.add("spring.rabbitmq.port", () -> rabbitMQ.getMappedPort(5672));
  }
}
