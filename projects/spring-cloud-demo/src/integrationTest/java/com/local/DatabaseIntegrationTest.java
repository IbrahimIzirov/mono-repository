package com.local;

import com.local.cloudnotification.CloudNotificationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;

@SpringBootTest
@AutoConfigureDataJpa
class DatabaseIntegrationTest implements DatabaseContainer {

  @TestConfiguration
  // Often when you're testing some service with connection to database here you have to put the name.
  @ComponentScan(basePackageClasses = CloudNotificationService.class)
  static class TestConfig {

  }

  @Autowired
  private WebTestClient webTestClient;

  @Autowired
  private CloudNotificationService cloudNotificationService;

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @BeforeEach
  void setUp() {
    jdbcTemplate.execute("delete from people");
    jdbcTemplate.execute("delete from phone_numbers");
    // Or you can use directly service here -> for example: outlierService.createOutlier(outlier);
    // Or outlierRepository.deleteAll();
  }

  @Test
  void getAll() {
    final var response = webTestClient.get()
      .uri(uriBuilder -> uriBuilder
        .path("/api/v1/test-url")
        .queryParam("offset", "10")
        .build())
      .exchange()
      .expectStatus().isOk()
      // Here you have to define your return type of controller instead of Object.
      .expectBodyList(Object.class)
      .returnResult().getResponseBody();

    // That assertion will assert objects from response and expected by ignore fields ID.
    assertThat(response)
      .usingRecursiveFieldByFieldElementComparatorIgnoringFields("id")
      // Instead of anyString you have to defined your expected class here.
      .containsExactly(anyString());

    assertThat(response).usingRecursiveComparison()
      .isEqualTo(anyString());
  }
}
