package com.local;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.local.stream.CloudNotification;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@SpringBootTest
@AutoConfigureWebTestClient
class CloudNotificationIntegrationTest implements DatabaseContainer {

  @Autowired
  private RabbitTemplate rabbitTemplate;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Test
  void triggerServiceThatPushToCloudNotification() throws JsonProcessingException {
    var messageAsString = objectMapper.writeValueAsString(
      List.of(
        generateMessage("OK", 1255, "Spain"),
        generateMessage("OK", 125, "Italy"),
        generateMessage("OK", 404, "Germany"),
        generateMessage("Waiting", 1241, "France"),
        generateMessage("Not yet", 9912, "Finland"),
        generateMessage("OK", 124, "Bulgaria")
      )
    );

    var message = MessageBuilder.withBody(messageAsString.getBytes())
      .andProperties(MessagePropertiesBuilder.newInstance().setContentType("application/json").build()).build();

    rabbitTemplate.convertAndSend("cloud-notification.cloud-service", message);

    await().atMost(5, TimeUnit.SECONDS).untilAsserted(() -> {
      List<TestCloudNotification> result = getTestCloudNotifications();
      assertThat(result).containsAnyElementsOf(Collections.singleton(
        new TestCloudNotification("OK", 404, "Germany")));
    });
  }

  private CloudNotification generateMessage(String status, long receive, String order) {
    return new CloudNotification(
      status,
      receive,
      order
    );
  }

  private List<TestCloudNotification> getTestCloudNotifications() {
    return jdbcTemplate.query(
      "select * from notifications",
      (rs, rowNum) -> new TestCloudNotification(
        rs.getString(1),
        rs.getLong(2),
        rs.getString(3)
      )
    );
  }

  private static class TestCloudNotification {
    private String status;
    private long receive;
    private String order;

    TestCloudNotification(final String status, final long receive, final String order) {
      this.status = status;
      this.receive = receive;
      this.order = order;
    }
  }
}
