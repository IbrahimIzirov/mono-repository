package com.local;

import java.time.LocalDateTime;
import java.util.function.Supplier;
import java.util.random.RandomGenerator;

public class Generator {
  private static final RandomGenerator random = RandomGenerator.getDefault();

  private Generator() {
  }

  public static long randomLong() {
    return Math.abs(random.nextLong());
  }

  public static int randomInt() {
    return Math.abs(random.nextInt());
  }

  public static float randomFloat() {
    return Math.abs(random.nextFloat());
  }

  public static double randomDouble() {
    return Math.abs(random.nextDouble());
  }

  public static boolean randomBoolean() {
    return random.nextBoolean();
  }

  public static String randomString() {
    return Integer.toHexString(LocalDateTime.now().hashCode() + randomInt());
  }

  public static <E extends Enum<E>> E randomEnum(Class<E> from) {
    final E[] values = from.getEnumConstants();
    return values[random.nextInt(values.length)];
  }

  public static <T> T randomNullable(Supplier<T> supplier) {
    if (randomBoolean()) return supplier.get();
    else return null;
  }
}
