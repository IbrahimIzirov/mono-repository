package com.local.stream.producer;

import com.local.stream.CloudNotification;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.function.Supplier;

@Configuration
class CloudNotificationProducerQueueConfiguration {

  private final Sinks.Many<CloudNotification> bufferCat = Sinks.many().multicast().onBackpressureBuffer();

  @Bean
  CloudNotificationProducerStreamService cloudNotificationProducerStreamService() {
    return bufferCat::tryEmitNext;
  }

  @Bean("cloud-notification-producer")
  Supplier<Flux<CloudNotification>> qCloudNotificationRequestDeclaration() {
    return bufferCat::asFlux;
  }
}
