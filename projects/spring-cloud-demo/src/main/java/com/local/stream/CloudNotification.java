package com.local.stream;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Data
@Getter
@Setter
public class CloudNotification {
  private String status;
  private long receive;
  private String order;

  public CloudNotification(final String status, final long receive, final String order) {
    this.status = status;
    this.receive = receive;
    this.order = order;
  }
}
