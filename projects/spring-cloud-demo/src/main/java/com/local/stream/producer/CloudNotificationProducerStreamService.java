package com.local.stream.producer;

import com.local.stream.CloudNotification;

public interface CloudNotificationProducerStreamService {

  void fire(final CloudNotification notification);
}
