package com.local.stream.consumer;

import com.local.cloudnotification.CloudNotificationService;
import com.local.stream.CloudNotification;
import com.local.utils.stream.RetryableConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;

import java.util.List;

@Configuration
class CloudNotificationQueueConfiguration {

  private static final Logger log = LoggerFactory.getLogger(CloudNotificationQueueConfiguration.class);

  @Bean("cloud-notification")
  RetryableConsumer<Message<List<CloudNotification>>> cloudNotificationListener(final CloudNotificationService cloudNotificationService) {
    return message -> {
      final var cloudNotifications = message.getPayload();
      cloudNotifications.forEach(cloudNotification -> cloudNotificationService.processNotification(cloudNotification));
    };
  }
}
