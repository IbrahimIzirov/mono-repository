package com.local.supplier;

import com.local.contract.Operation;
import com.local.stream.CloudNotification;

import java.util.List;

public interface ProductDataSupplier {

  List<Integer> getSales();

  List<CloudNotification> getNotifications();

  List<Operation> getOperations();

  String getOperationCompany();
}
