package com.local.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serial;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.LongFunction;

/**
 * When fromm some service this exception was thrown, then it's not required to have catch clause anywhere
 * it will produce 404. (By this code example you can eliminate GlobalExceptionHandler class)
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

  @Serial
  private static final long serialVersionUID = 7120661755951396876L;

  EntityNotFoundException(final String message) {
    super(message);
  }

  private static final String OBJECT_NOT_FOUND_BY_ID = "Object by id: %s was not found!";
  public static final LongFunction<EntityNotFoundException> OBJECT_NOT_FOUND = id -> new EntityNotFoundException(OBJECT_NOT_FOUND_BY_ID.formatted(id));
  private static final String ITEM_NOT_FOUND_BY_ID = "Item by id: %s was not found!";
  public static final IntFunction<EntityNotFoundException> ITEM_NOT_FOUND = id -> new EntityNotFoundException(OBJECT_NOT_FOUND_BY_ID.formatted(id));
  private static final String PARTIAL_NOT_FOUND_BY_NAME = "Partial name: %s was not found!";
  public static final Function<String, EntityNotFoundException> PARTIAL_NOT_FOUND = name -> new EntityNotFoundException(OBJECT_NOT_FOUND_BY_ID.formatted(name));

  // Convenient here is when you are applying that exception just use:
  // .orElseThrow(() -> OBJECT_NOT_FOUND.apply(objectId);
}
