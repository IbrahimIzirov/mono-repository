package com.local.cloudnotification.internal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CloudNotificationRepository extends JpaRepository<CloudNotificationEntity, Long> {

}
