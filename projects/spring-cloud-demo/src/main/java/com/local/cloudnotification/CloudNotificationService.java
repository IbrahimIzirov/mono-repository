package com.local.cloudnotification;

import com.local.supplier.ProductDataSupplier;
import com.local.stream.CloudNotification;

public interface CloudNotificationService {

  void processNotification(CloudNotification notification);

  ProductDataSupplier getProductDataSupplier(final int id, final long companyId);
}
