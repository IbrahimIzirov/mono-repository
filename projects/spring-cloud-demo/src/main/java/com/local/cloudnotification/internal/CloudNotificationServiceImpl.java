package com.local.cloudnotification.internal;

import com.local.cloudnotification.CloudNotificationService;
import com.local.contract.CompanyUYT;
import com.local.contract.JobStatus;
import com.local.contract.Operation;
import com.local.monitoring.MetricsNotification;
import com.local.monitoring.MetricsNotificationRequestStreamService;
import com.local.stream.CloudNotification;
import com.local.supplier.ProductDataSupplier;
import com.local.utils.ElasticLogContext;
import com.local.utils.exceptions.DatabaseRetryableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.CannotCreateTransactionException;

import java.util.List;
import java.util.Map;

@Service
class CloudNotificationServiceImpl implements CloudNotificationService {

  private MetricsNotificationRequestStreamService metricsRequestStreamService;

  @Autowired
  CloudNotificationServiceImpl(final MetricsNotificationRequestStreamService metricsRequestStreamService) {
    this.metricsRequestStreamService = metricsRequestStreamService;
  }

  @Override
  public void processNotification(final CloudNotification notification) {
    //This guy will be responsible for viewing additonal parameters in elastic search when you open json properties.
    ElasticLogContext.put(new CompanyUYT(2, 1, 3, new CompanyUYT.JobStage(null, "21", JobStatus.RELEASED)));
    metricsRequestStreamService.apply(new MetricsNotification(1, 2, "cloud-service",
      Map.of("duration", 7.0120)));

    try {
      // some database operation.
      // Keep in mind if that method is @Transactional, then catching directly here CannotCreateTransactionException is not working.
      // You need to catch that exception in upper level.
    } catch (CannotCreateTransactionException e) {
      throw new DatabaseRetryableException(e);
    }
  }

  // this supplier will produce multiple elements from one interface.
  // now if you go to another service, inject this one (CloudNotificationService) with injection
  // and at later point you can receive information through final var productDataSupplier = service.getProductDataSupplier(1,2)
  // That way, this supplier will contain all of your 4 elements at the same time.
  @Override
  public ProductDataSupplier getProductDataSupplier(final int id, final long companyId) {
    // Do some thins with information here.
    final var sales = List.of(1, 2, 3, 4, 5);
    final var notification = new CloudNotification("received", 22L, "order");
    final var operation = new Operation(1, 2.2, 3.3, 4, "test", "test", "test", null);

    return new ProductDataSupplier() {
      @Override
      public List<Integer> getSales() {
        return sales;
      }

      @Override
      public List<CloudNotification> getNotifications() {
        return List.of(notification);
      }

      @Override
      public List<Operation> getOperations() {
        return List.of(operation);
      }

      @Override
      public String getOperationCompany() {
        return operation.channel();
      }
    };
  }
}
