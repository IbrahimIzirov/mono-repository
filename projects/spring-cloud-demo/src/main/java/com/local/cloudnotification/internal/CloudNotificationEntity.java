package com.local.cloudnotification.internal;

import io.hypersistence.utils.hibernate.type.array.ListArrayType;
import io.hypersistence.utils.hibernate.type.json.JsonType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.PartitionKey;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@Setter(AccessLevel.PACKAGE)
@Getter(AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PACKAGE, staticName = "of")
@Entity
@EqualsAndHashCode
class CloudNotificationEntity implements Serializable {

  @Serial
  private static final long serialVersionUID = -8472860731058206314L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private long id;

  @Column(name = "delivery_type_id")
  private Integer deliveryTypeId;

  @Column(name = "shop_id")
  private Long shopId;

  @PartitionKey
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
  @JoinColumn(name = "product_group_id", nullable = false)
  private ProductGroupEntity productGroup;

  @Column(name = "reason")
  private String reason;

  // by adding this field, in database we can store different type of information in jsonb column.
  // {"pg": 62, "countryName": "EN", "type_label": "W"}
  @Type(JsonType.class)
  @Column(name = "properties")
  private Map<String, Object> properties;

  // by adding this field, in database we can store information in type array column.
  // {"periods": [62,712,951,156,125]}
  @Type(ListArrayType.class)
  @Column(name = "periods")
  private Set<Integer> periods;

  @CreationTimestamp
  @Column(name = "created")
  private LocalDateTime created;

  @UpdateTimestamp
  @Column(name = "updated")
  private LocalDateTime updated;
}
