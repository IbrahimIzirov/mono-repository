package com.local.cloud;

import com.local.metrics.CounterService;
import com.local.metrics.Metric;
import com.local.metrics.MetricsFactory;
import com.local.metrics.MetricsMeta;
import com.local.stream.CloudNotification;
import com.local.stream.producer.CloudNotificationProducerStreamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class Controller {

  private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);

  private final CloudNotificationProducerStreamService cloudNotificationProducerStreamService;
  private final CounterService counterMetricsService;

  @Autowired
  public Controller(final CloudNotificationProducerStreamService cloudNotificationProducerStreamService,
                    final MetricsFactory metricsFactory) {
    this.cloudNotificationProducerStreamService = cloudNotificationProducerStreamService;
    this.counterMetricsService = metricsFactory.createCounter(Metric.of("number_of_retrieval_objects", "Number of objects retrieval from queue"));
  }

  @GetMapping("hello")
  public String getMessage() {
    LOGGER.info("Sending value {} to topic", "hello from Ibrahim!");
    cloudNotificationProducerStreamService.fire(new CloudNotification("OK", 200, "Number1214"));
    counterMetricsService.count(new MetricsMeta(14124, 91219), 1);
    return "hello from Ibrahim!";
  }
}
