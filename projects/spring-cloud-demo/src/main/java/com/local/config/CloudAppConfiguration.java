package com.local.config;

import com.local.metrics.MetricsConfiguration;
import com.local.monitoring.EnableMonitoring;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({MetricsConfiguration.class})
@EnableMonitoring
class CloudAppConfiguration {
}
