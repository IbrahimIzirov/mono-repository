create table people (
  person_id         int not null auto_increment primary key,
  first_name        varchar(20),
  last_name         varchar(20),
  phone_number      varchar(20),
  address_line_1    varchar(20),
  address_line_2    varchar(100),
  city              varchar(20),
  state             varchar(2),
  zip_code          varchar(5)
);

create table states (
  state             varchar(2) not null primary key,
  description       varchar(20)
);

create table email (
  email_id          int not null auto_increment primary key,
  email_address     varchar(100),
  email_type        char(1) not null,
  person_id         int not null
);

create table phone_numbers (
  phone_number_id   int not null auto_increment primary key,
  phone_number      varchar(20),
  phone_type        char(1),
  person_id         int not null
);

alter table people add constraint fk_people_state
  foreign key (state) references states(state);

alter table phone_numbers add constraint fk_phone_numbers_people
  foreign key (person_id) references people(person_id);