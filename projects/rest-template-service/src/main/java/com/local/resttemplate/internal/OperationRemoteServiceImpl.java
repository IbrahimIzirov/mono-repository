package com.local.resttemplate.internal;

import com.local.contract.Operation;
import com.local.contract.OperationFilter;
import com.local.contract.page.PageResponse;
import com.local.resttemplate.OperationRemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

import static com.local.resttemplate.exception.EntityNotFoundException.OPERATION_NOT_FOUND;

@Service
class OperationRemoteServiceImpl extends RemoteServiceBase implements OperationRemoteService {

  private static final ParameterizedTypeReference<List<Operation>> OPERATION_TYPE = new ParameterizedTypeReference<>() {
  };

  @Autowired
  OperationRemoteServiceImpl(@Qualifier("operationRestTemplate") final RestTemplate restTemplate) {
    super(restTemplate);
  }

  @Override
  public Operation getOperationById(final long id) {
    return get("/operation/%d".formatted(id), Operation.class)
      .orElseThrow(() -> OPERATION_NOT_FOUND.apply(id));
  }

  @Override
  public List<Operation> getListOfOperations(final int companyId, final int periodId) {
    return get("/operations/grouped?companyId={companyId}&periodId={periodId}",
      OPERATION_TYPE, Map.of("companyId", companyId, "periodId", periodId));
  }

  @Override
  public PageResponse getOperationsPage(final OperationFilter filter) {
    return post("/operations", PageResponse.class, filter);
  }
}
