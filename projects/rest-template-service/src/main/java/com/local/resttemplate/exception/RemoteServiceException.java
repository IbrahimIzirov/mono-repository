package com.local.resttemplate.exception;

import lombok.experimental.StandardException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serial;

@StandardException
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class RemoteServiceException extends RuntimeException {
  @Serial
  private static final long serialVersionUID = 1383896091956853092L;
}
