package com.local.resttemplate.internal;

import com.local.resttemplate.exception.RemoteServiceException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

class RemoteServiceResponseErrorHandler implements ResponseErrorHandler {

  @Override
  public boolean hasError(final ClientHttpResponse response) throws IOException {
    return response.getStatusCode().is5xxServerError() ||
      response.getStatusCode().is4xxClientError();
  }

  @Override
  public void handleError(final ClientHttpResponse response) throws IOException {
    if (response.getStatusCode().is4xxClientError()) {
      throw new RemoteServiceException();
    }
  }
}
