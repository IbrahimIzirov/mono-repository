package com.local.resttemplate.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableConfigurationProperties(RemoteServiceProperties.class)
class RemoteServiceConfiguration {

  private final RemoteServiceProperties properties;

  @Autowired
  RemoteServiceConfiguration(final RemoteServiceProperties properties) {
    this.properties = properties;
  }

  @Bean("operationRestTemplate")
  RestTemplate operationRestTemplate(RestTemplateBuilder builder) {
    return builder.rootUri(properties.operation().toString()).build();
  }

  @Bean("anotherServiceRestTemplate")
  RestTemplate anotherServiceRestTemplate(RestTemplateBuilder builder) {
    return builder.rootUri(properties.anotherService().toString()).build();
  }
}
