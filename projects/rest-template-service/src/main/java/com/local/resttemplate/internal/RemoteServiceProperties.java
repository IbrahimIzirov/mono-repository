package com.local.resttemplate.internal;

import org.springframework.boot.context.properties.ConfigurationProperties;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.net.URI;

@Valid
@ConfigurationProperties(prefix = "remote-endpoint")
record RemoteServiceProperties(
  // full path to exposed interface in cluster.
  @NotNull URI operation,
  @NotNull URI anotherService
) {
}
