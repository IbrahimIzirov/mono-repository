package com.local.resttemplate.config;

import com.local.swagger.EnableSwaggerUI;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableSwaggerUI
class ServiceApplicationConfig {
}
