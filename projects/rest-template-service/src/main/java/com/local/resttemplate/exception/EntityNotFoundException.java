package com.local.resttemplate.exception;

import lombok.experimental.StandardException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serial;
import java.util.function.LongFunction;

@StandardException
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {
  @Serial
  private static final long serialVersionUID = -5212408507502485980L;

  public EntityNotFoundException(String message) {}

  private static final String OPERATION_NOT_FOUND_BY_ID = "Operation not found by id: %s";

  public static final LongFunction<EntityNotFoundException> OPERATION_NOT_FOUND = id ->
    new EntityNotFoundException(OPERATION_NOT_FOUND_BY_ID.formatted(id));
}
