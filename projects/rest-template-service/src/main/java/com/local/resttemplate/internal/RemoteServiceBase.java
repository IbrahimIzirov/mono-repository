package com.local.resttemplate.internal;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;

abstract class RemoteServiceBase {

  private final RestTemplate restTemplate;

  RemoteServiceBase(final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
    this.restTemplate.setErrorHandler(new RemoteServiceResponseErrorHandler());
  }

  static HttpHeaders headers() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return headers;
  }

  protected <T> T get(final String path, final ParameterizedTypeReference<T> responseType, final Map<String, ?> uriVariables) {
    final HttpEntity<T> entity = new HttpEntity<>(headers());
    final ResponseEntity<T> response = restTemplate.exchange(path, HttpMethod.GET, entity, responseType, uriVariables);
    return response.getBody();
  }

  protected <T> Optional<T> get(final String path, Class<T> responseType) {
    final HttpEntity<T> entity = new HttpEntity<>(headers());
    final ResponseEntity<T> response = restTemplate.exchange(path, HttpMethod.GET, entity, responseType);
    return Optional.ofNullable(response.getBody());
  }

  protected <B, R> R post(final String path, Class<R> responseType, final B body) {
    final HttpEntity<B> entity = new HttpEntity<>(body, headers());
    final ResponseEntity<R> response = restTemplate.exchange(path, HttpMethod.POST, entity, responseType);
    return response.getBody();
  }
}
