package com.local.resttemplate;

import com.local.contract.Operation;
import com.local.contract.OperationFilter;
import com.local.contract.page.PageResponse;

import java.util.List;

public interface OperationRemoteService {
  Operation getOperationById(long id);

  List<Operation> getListOfOperations(int companyId, int periodId);

  PageResponse getOperationsPage(OperationFilter filter);
}
