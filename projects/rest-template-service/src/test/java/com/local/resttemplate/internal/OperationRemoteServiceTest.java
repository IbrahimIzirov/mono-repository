package com.local.resttemplate.internal;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.matching.StringValuePattern;
import com.local.contract.Operation;
import com.local.contract.OperationFilter;
import com.local.contract.page.PageResponse;
import com.local.resttemplate.FileLoader;
import com.local.resttemplate.OperationRemoteService;
import com.local.resttemplate.WireMockServer;
import com.local.resttemplate.exception.RemoteServiceException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.client.RestTemplateAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.io.IOException;
import java.net.URI;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.local.resttemplate.Fixtures.defaultOperation;
import static com.local.resttemplate.Fixtures.defaultOperationsPage;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@SpringJUnitConfig({RestTemplateAutoConfiguration.class, OperationRemoteServiceTest.TestConfig.class})
class OperationRemoteServiceTest implements WireMockServer {

  @Autowired
  OperationRemoteService operationRemoteService;

  @AfterEach
  void afterEach() {
    wireMockServer.resetAll();
  }

  @Test
  void checkContractOperations() throws IOException {
    preparePost("/operations", FileLoader.loadFileContent("mocked-operations-page-response.json").getBytes());
    final PageResponse result = operationRemoteService.getOperationsPage(new OperationFilter());
    assertThat(defaultOperationsPage()).isEqualTo(result);
  }

  @Test
  void checkErrorOperations() {
    assertThatThrownBy(() -> {
      preparePost("/no-existing-endpoint", null);
      operationRemoteService.getOperationsPage(new OperationFilter());
    }).isInstanceOf(RemoteServiceException.class);
  }

  @Test
  void checkContractOperationById() throws IOException {
    prepareGet("/operation/%d".formatted(2L), Map.of(), FileLoader.loadFileContent("mocked-operation-by-id-response.json").getBytes());
    final Operation result = operationRemoteService.getOperationById(2L);
    assertThat(defaultOperation()).isEqualTo(result);
  }

  @Test
  void checkContractGroupedOperations() throws IOException {
    final var operation = defaultOperation();
    final int companyId = 21;
    final int periodId = 65;

    final Map<String, ?> params = Map.of(
      "companyId", String.valueOf(companyId),
      "periodId", String.valueOf(periodId)
    );

    prepareGet("/operations/grouped", params, FileLoader.loadFileContent("mocked-operation-grouped-response.json").getBytes());
    final List<Operation> result = operationRemoteService.getListOfOperations(companyId, periodId);
    assertThat(result).containsExactly(operation);
  }

  protected void preparePost(final String path, final byte[] body) {
    prepare(post(path), body);
  }

  protected void prepareGet(final String path, Map<String, ?> queryParams, final byte[] body) {
    prepare(get(urlPathEqualTo(path)).withQueryParams(params(queryParams)), body);
  }

  private void prepare(final MappingBuilder mb, final byte[] body) {
    wireMockServer.stubFor(mb.willReturn(aResponse().withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE).withBody(body)));
  }

  private Map<String, StringValuePattern> params(final Map<String, ?> queryParams) {
    return queryParams.entrySet().stream()
      .map(entry -> new AbstractMap.SimpleEntry<>(entry.getKey(), equalTo(String.valueOf(entry.getValue()))) {
      })
      .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  @TestConfiguration
  static class TestConfig {

    @Bean
    RemoteServiceConfiguration remoteServiceConfiguration() {
      return new RemoteServiceConfiguration(new RemoteServiceProperties(URI.create(wireMockServer.baseUrl()), URI.create(wireMockServer.baseUrl())));
    }

    @Bean
    OperationRemoteService operationRemoteService(final RestTemplateBuilder builder, final RemoteServiceConfiguration configuration) {
      return new OperationRemoteServiceImpl(remoteServiceConfiguration().operationRestTemplate(builder));
    }
  }
}
