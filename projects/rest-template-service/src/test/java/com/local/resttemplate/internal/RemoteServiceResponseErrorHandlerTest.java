package com.local.resttemplate.internal;

import com.local.contract.Operation;
import com.local.resttemplate.exception.RemoteServiceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import static com.local.resttemplate.Fixtures.defaultOperation;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;

@ExtendWith(MockitoExtension.class)
class RemoteServiceResponseErrorHandlerTest {

  private static final String URL = "/v1/outlier";
  private MockRestServiceServer mockRestServiceServer;
  private RestTemplate restTemplate;

  @BeforeEach
  void setUp() {
    restTemplate = new RestTemplate();
    restTemplate.setErrorHandler(new RemoteServiceResponseErrorHandler());
    mockRestServiceServer = MockRestServiceServer.createServer(restTemplate);
  }

  @Test
  void response4xx() {
    mockRestServiceServer
      .expect(requestTo(URL))
      .andExpect(method(GET))
      .andRespond(withBadRequest());

    ResponseEntity<Operation> response = new ResponseEntity<>(BAD_REQUEST);
    try {
      response = consumerWebService();
    } catch (RemoteServiceException ignored) {
    }
    assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
  }

  @Test
  void response4xxAndThrowExceptionFromHandler() {
    mockRestServiceServer
      .expect(requestTo(URL))
      .andExpect(method(GET))
      .andRespond(withBadRequest());

    assertThrows(RemoteServiceException.class, this::consumerWebService);
  }

  @Test
  void response5xx() {
    mockRestServiceServer
      .expect(requestTo(URL))
      .andExpect(method(GET))
      .andRespond(withServerError());

    ResponseEntity<Operation> response = consumerWebService();
    assertThat(response.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
  }

  ResponseEntity<Operation> consumerWebService() {
    try {
      return restTemplate.getForEntity(URL, Operation.class);
    } catch (RestClientResponseException e) {
      return ResponseEntity.status(e.getStatusCode()).body(defaultOperation());
    }
  }
}
