package com.local.resttemplate;

import com.local.contract.Operation;
import com.local.contract.page.PageResponse;

import java.util.List;

public class Fixtures {

  public static Operation defaultOperation() {
    return new Operation(
      21,
      22.2,
      12.7,
      99129,
      "Some description for dashboard",
      "LG",
      "PIPE_TOWER",
      42
    );
  }

  public static PageResponse defaultOperationsPage() {
    return new PageResponse(
      56, 6, List.of(defaultOperation())
    );
  }
}
