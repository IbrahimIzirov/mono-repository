package com.local.resttemplate;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileLoader {

  public static String loadFileContent(final String path) throws IOException {
    final File file = new ClassPathResource(path).getFile();
    return new String(Files.readAllBytes(file.toPath()));
  }
}
