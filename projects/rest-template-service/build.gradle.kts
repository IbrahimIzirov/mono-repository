plugins {
  id("spring-boot-dependencies")
}

group = "com.local"

dependencies {
  implementation(project(":model-contracts"))
  implementation(project(":lib-swagger-ui"))
  implementation("org.springframework.boot:spring-boot-starter-actuator")
  implementation("org.springframework.boot:spring-boot-starter-validation")
  implementation("org.springframework.boot:spring-boot-starter-web")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("org.assertj:assertj-core")
  testImplementation("com.squareup.okhttp3:mockwebserver")
  testImplementation("org.wiremock:wiremock-standalone")
  testRuntimeOnly("com.squareup.okhttp3:okhttp")
}
