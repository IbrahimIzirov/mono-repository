package com.console.spring.app.cli;

import com.console.spring.app.service.ApplicationService;
import org.jline.terminal.Terminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@ShellComponent
public class Commands {

  final Terminal terminal;
  final ApplicationService applicationService;


  @Autowired
  public Commands(final Terminal terminal, final ApplicationService applicationService) {
    this.terminal = terminal;
    this.applicationService = applicationService;

    this.terminal.writer().println("Welcome to our console application");
    this.terminal.writer().println("type `help` to see all commands.");
    this.terminal.writer().println("type `help command-name` to see all available options.");
    this.terminal.flush();
  }

  @ShellMethod(key = {"-i", "--import"}, value = "Import parameters from input")
  public void executeCommandByTwoParameters(final int parameterOne, final int parameterTwo) {
    this.applicationService.doSomething(parameterOne, parameterTwo);
  }

  @ShellMethod(key = {"-l", "--list"}, value = "Import parameters from list of input")
  public void executeCommandByListOfParameters(final int[] parameters) {

    // With this operations you made the application asynchronous, because it will work parallel.
    final List<CompletableFuture<Void>> futures = Arrays.stream(parameters)
      .mapToObj(parameter -> CompletableFuture.runAsync(() -> this.applicationService.doSomething(parameter)))
      .toList();

    CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();
  }
}
