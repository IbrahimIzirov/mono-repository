package com.console.spring.app.service;

public interface ApplicationService {

  void doSomething(final int parameterOne, final int parameterTwo);

  void doSomething(final int parameter);
}
