package com.console.spring.app.service;

import org.springframework.stereotype.Service;

@Service
public class ApplicationServiceImpl implements ApplicationService {


  @Override
  public void doSomething(final int parameterOne, final int parameterTwo) {
    // Do something with that console input
  }

  @Override
  public void doSomething(final int parameter) {
    // Do something with one parameter
  }
}
