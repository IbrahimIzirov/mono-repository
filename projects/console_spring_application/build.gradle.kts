plugins {
  id("spring-boot-conventions")
}

group = "com.local"

configurations {
  compileOnly {
    extendsFrom(configurations.annotationProcessor.get())
  }
}

extra["shellVersion"] = "3.1.4"

dependencies {
  implementation("org.springframework.boot:spring-boot-starter-actuator")
  implementation("org.springframework.shell:spring-shell-starter")
  annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
}

dependencyManagement {
  imports {
    mavenBom("org.springframework.shell:spring-shell-dependencies:${property("shellVersion")}")
  }
}
