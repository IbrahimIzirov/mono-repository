package com.local.utils.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ObjectMapperConfig {

  @Primary
  @Bean
  public static ObjectMapper objectMapper() {
    final ObjectMapper objectMapper = new ObjectMapper();
    configObjectMapper(objectMapper);
    return objectMapper;
  }

  private static void configObjectMapper(ObjectMapper objectMapper) {
    objectMapper.registerModule(new Jdk8Module()).registerModule(new JavaTimeModule());

    objectMapper.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, false);
    objectMapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
    objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

    // to be able to parse parameters which could be a string or string[]
    objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, false);
    objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, false);
  }
}
