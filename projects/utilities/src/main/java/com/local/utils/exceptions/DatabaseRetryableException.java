package com.local.utils.exceptions;

import java.io.Serial;

public class DatabaseRetryableException extends RetryableException {
  @Serial
  private static final long serialVersionUID = 2564211285359284042L;

  public DatabaseRetryableException(final String message) {
    super(message);
  }

  public DatabaseRetryableException(final Throwable cause) {
    super(cause);
  }
}
