package com.local.utils;

import jakarta.annotation.Nullable;
import java.nio.ByteBuffer;
import java.time.ZonedDateTime;
import java.util.function.Supplier;
import java.util.random.RandomGenerator;

public class Generator {
  private static final RandomGenerator random = RandomGenerator.getDefault();

  private Generator() {
  }

  public static long randomLong() {
    return Math.abs(random.nextLong());
  }

  public static int randomInt() {
    return Math.abs(random.nextInt());
  }

  public static int randomSmallInt() {
    return Math.abs(random.nextInt(0, 6));
  }

  public static float randomFloat() {
    return Math.abs(random.nextFloat());
  }

  public static double randomDouble() {
    return Math.abs(random.nextDouble());
  }

  public static boolean randomBoolean() {
    return random.nextBoolean();
  }

  public static String randomString() {
    return Integer.toHexString(ZonedDateTime.now().hashCode() + randomInt());
  }

  public static byte[] randomBytes() {
    return randomString().getBytes();
  }

  public static <E extends Enum<E>> E randomEnum(Class<E> enumClas) {
    final E[] values = enumClas.getEnumConstants();
    return values[random.nextInt(values.length)];
  }

  @Nullable
  public static <T> T randomNullable(Supplier<T> supplier) {
    if (randomBoolean()) return supplier.get();
    else return null;
  }

  @Nullable
  public static Object randomValue() {
    return switch (randomSmallInt()) {
      case 0 -> randomInt();
      case 1 -> randomLong();
      case 2 -> randomFloat();
      case 3 -> randomDouble();
      case 4 -> randomString();
      case 5 -> ByteBuffer.wrap(randomBytes());
      default -> null;
    };
  }
}
