package com.local.utils.stream;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.local.utils.config.ObjectMapperConfig;
import com.local.utils.exceptions.RetryableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;

import java.util.function.Consumer;

@FunctionalInterface
public interface RetryableConsumer<T> extends Consumer<T> {
  Logger log = LoggerFactory.getLogger(RetryableConsumer.class);
  ObjectMapper mapper = ObjectMapperConfig.objectMapper();

  default void accept(T message) {
    try {
      handlePayload(message);
    } catch (RetryableException re) {
      log.error("Retry attempt: ", re);
      throw re;
    } catch (Exception e) {
      if (message instanceof final Message<?> msg) {
        Object payload = msg.getPayload();
        log.error("Couldn't process payload: {}", printJsonPayload(payload), e);
      }
    }
  }

  void handlePayload(T payload) throws Exception;

  private String printJsonPayload(Object payload) {
    try {
      if (payload instanceof String) return mapper.readTree((String) payload).toPrettyString();
      else if (payload instanceof byte[]) return mapper.readTree(new String((byte[]) payload)).toPrettyString();
      else return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(payload);
    } catch (Exception e) {
      return "";
    }
  }
}
