package com.local.utils.exceptions;

import java.io.Serial;

public class RabbitRetryableException extends RetryableException {
  @Serial
  private static final long serialVersionUID = 7561223245152214995L;

  public RabbitRetryableException(final String message) {
    super(message);
  }

  public RabbitRetryableException(final Throwable cause) {
    super(cause);
  }
}
