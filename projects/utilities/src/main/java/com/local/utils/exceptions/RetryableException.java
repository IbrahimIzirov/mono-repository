package com.local.utils.exceptions;

import java.io.Serial;

public abstract class RetryableException extends RuntimeException {

  @Serial
  private static final long serialVersionUID = -5560111386705301490L;

  public RetryableException() {
    super();
  }

  public RetryableException(final String message) {
    super(message);
  }

  public RetryableException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public RetryableException(final Throwable cause) {
    super(cause);
  }
}
