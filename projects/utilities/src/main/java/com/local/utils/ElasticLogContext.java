package com.local.utils;

import ch.qos.logback.classic.util.LogbackMDCAdapter;
import com.local.contract.CompanyUYT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import jakarta.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ElasticLogContext {
  private static final Logger log = LoggerFactory.getLogger(ElasticLogContext.class);

  private static final String COMPANY_ID = "companyId";
  private static final String COUNTRY_ID = "countryId";
  private static final String PERIOD_ID = "periodId";
  private static final String JOB_STAGE_STATUS = "jobStageStatus";
  private static final String STAGE = "stage";

  private ElasticLogContext() {
  }

  static {
    if (MDC.getMDCAdapter() instanceof LogbackMDCAdapter) {
      log.info("MDC is initialised properly, the child thread automatically inherits a copy of its parent's MDC");
    } else {
      log.error("Unsupported MDC adapter detected, this may come from 3rd party dependency.\n" +
        "The mapped diagnostic context may work in ways not expected to be.");
    }
  }

  public static void put(CompanyUYT companyData) {
    apply(contextMap(companyData, null));
  }

  public static void put(CompanyUYT companyData, String stage) {
    apply(contextMap(companyData, stage));
  }

  private static Map<String, String> contextMap(CompanyUYT companyData, @Nullable String stage) {
    Map<String, String> contextMap = new HashMap<>();
    contextMap.put(COMPANY_ID, String.valueOf(companyData.companyId()));
    contextMap.put(COUNTRY_ID, String.valueOf(companyData.countryId()));
    contextMap.put(PERIOD_ID, String.valueOf(companyData.periodId()));

    Optional.ofNullable(companyData.jobStage().status()).ifPresent(value ->
      contextMap.put(JOB_STAGE_STATUS, companyData.jobStage().status().name())
    );

    Optional.ofNullable(stage).ifPresent(value -> contextMap.put(STAGE, stage));
    return contextMap;
  }

  private static void apply(Map<String, String> params) {
    params.forEach(MDC::put);
  }
}
