plugins {
  id("spring-boot-dependencies")
}

group = "com.local"

dependencies {
  implementation(project(":model-contracts"))
  implementation("org.springframework:spring-context")
  implementation("org.springframework.boot:spring-boot-starter-validation")
  implementation("org.springframework.cloud:spring-cloud-function-context")
  implementation("com.fasterxml.jackson.datatype:jackson-datatype-jdk8")
  implementation("com.fasterxml.jackson.core:jackson-databind")
  implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
}
