package com.local.strategies.criteriabuilder;

import com.local.contract.OutlierStatus;
import com.local.contract.OutlierType;
import com.local.contract.Periodicity;
import com.local.strategies.entities.CorrectionEntity;
import com.local.strategies.entities.CorrectionEntity_;
import com.local.strategies.entities.CountryEntity_;
import com.local.strategies.entities.OutlierEntity;
import com.local.strategies.entities.OutlierEntity_;
import com.local.strategies.entities.PeriodEntity_;
import com.local.strategies.entities.ProductGroupEntity_;
import com.local.strategies.models.OutlierFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import jakarta.annotation.Nonnull;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import jakarta.persistence.criteria.Subquery;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.local.contract.OutlierCorrectionType.MACHINE;
import static com.local.strategies.entities.CorrectionEntity_.correctionType;
import static com.local.strategies.entities.CorrectionEntity_.created;
import static com.local.strategies.entities.CorrectionEntity_.outlierId;
import static com.local.strategies.entities.OutlierEntity_.companyName;
import static com.local.strategies.entities.OutlierEntity_.corrections;
import static com.local.strategies.entities.OutlierEntity_.gsnr;
import static com.local.strategies.entities.OutlierEntity_.modelText;
import static com.local.strategies.entities.OutlierEntity_.objectId;
import static com.local.strategies.entities.OutlierEntity_.outlierByPrice;
import static com.local.strategies.entities.OutlierEntity_.outlierBySalesVolume;
import static com.local.strategies.entities.OutlierEntity_.outlierByTurnover;
import static com.local.strategies.entities.OutlierEntity_.period;
import static com.local.strategies.entities.OutlierEntity_.priceOrg;
import static com.local.strategies.entities.OutlierEntity_.productGroup;
import static com.local.strategies.entities.OutlierEntity_.salesVolume;
import static com.local.strategies.entities.OutlierEntity_.status;
import static com.local.strategies.entities.OutlierEntity_.turnover;
import static com.local.strategies.entities.ProductGroupEntity_.country;

@Repository
class OutlierRepositoryCustomImpl implements OutlierRepositoryCustom {

  private final EntityManager entityManager;
  private final CriteriaBuilder builder;

  @Autowired
  OutlierRepositoryCustomImpl(final EntityManager entityManager) {
    this.entityManager = entityManager;
    this.builder = entityManager.getCriteriaBuilder();
  }

  @Override
  public Page<OutlierEntity> findOutliers(final Pageable pageable, final OutlierFilter outlierSearch) {
    return fillPeriodId(outlierSearch).map(
      periodId -> searchByParams(pageable, outlierSearch, periodId)
    ).orElse(Page.empty());
  }

  @Nonnull
  private Page<OutlierEntity> searchByParams(final Pageable pageable, final OutlierFilter outlierSearch, Integer periodId) {
    CriteriaQuery<OutlierEntity> query = builder.createQuery(OutlierEntity.class);
    Root<OutlierEntity> outlier = query.from(OutlierEntity.class);

    List<Predicate> predicates = getPredicates(outlierSearch, outlier, periodId, query);
    List<Order> orders = getOrders(pageable, outlier);

    query.where(predicates.toArray(new Predicate[0]));
    query.orderBy(orders);

    final var emQuery = entityManager.createQuery(query);
    emQuery.setFirstResult((int) pageable.getOffset());
    emQuery.setMaxResults(pageable.getPageSize());
    return new PageImpl<>(emQuery.getResultList(), pageable, getTotalCount(outlierSearch, periodId));
  }

  private long getTotalCount(final OutlierFilter outlierSearch, final int periodId) {
    CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);
    Root<OutlierEntity> countRoot = countQuery.from(OutlierEntity.class);

    // In Hibernate 6 it's no longer possible to reuse Predicates across different CriteriaQueries.
    List<Predicate> predicates = getPredicates(outlierSearch, countRoot, periodId, countQuery);
    countQuery.select(builder.count(countRoot)).where(predicates.toArray(new Predicate[0]));
    return entityManager.createQuery(countQuery).getSingleResult();
  }

  private Optional<Integer> fillPeriodId(final OutlierFilter outlierSearch) {
    final String fullPeriodString = outlierSearch.getPeriod();
    if (fullPeriodString.isEmpty()) return Optional.empty();
    final String periodString = fullPeriodString.substring(0, 4);
    final Periodicity periodicity = Periodicity.valueOf(fullPeriodString.substring(fullPeriodString.length() - 1));
    return Optional.ofNullable(2);
  }

  @Nonnull
  private <T> List<Predicate> getPredicates(final OutlierFilter outlierSearch, final Root<OutlierEntity> outlier, final int periodId, final CriteriaQuery<T> criteriaQuery) {
    List<Predicate> predicates = new ArrayList<>();
    Join<OutlierEntity, CorrectionEntity> correctionJoin = outlier.join(corrections, JoinType.INNER);

    predicates.add(builder.equal(outlier.get(productGroup).get(country).get(CountryEntity_.id), outlierSearch.getCountryId()));
    predicates.add(builder.equal(outlier.get(productGroup).get(ProductGroupEntity_.id), outlierSearch.getProductGroupId()));
    predicates.add(builder.equal(outlier.get(period).get(PeriodEntity_.periodId), periodId));
    predicates.add(builder.notEqual(outlier.get(status), OutlierStatus.NONE));

    if (outlierSearch.getType() != null) {
      switch (outlierSearch.getType()) {
        case TURNOVER -> predicates.add(builder.isTrue(outlier.get(outlierByTurnover)));
        case SALES_VOLUME -> predicates.add(builder.isTrue(outlier.get(outlierBySalesVolume)));
        case PRICE -> predicates.add(builder.isTrue(outlier.get(outlierByPrice)));
      }
    }


    if (outlierSearch.getDecidedBy() != null) {
      switch (outlierSearch.getDecidedBy()) {
        case USER -> predicates.add(builder.notEqual(correctionJoin.get(correctionType), MACHINE));
        case MACHINE -> {
          final var isEmptyPredicate = builder.isEmpty(outlier.get(corrections));
          final var correctionTypePredicate = builder.equal(correctionJoin.get(correctionType), MACHINE);
          predicates.add(builder.or(isEmptyPredicate, correctionTypePredicate));
        }
      }
    }

    if (outlierSearch.getJobStatuses() != null && !outlierSearch.getJobStatuses().isEmpty()) {
      Subquery<ZonedDateTime> subquery = criteriaQuery.subquery(ZonedDateTime.class);
      Root<CorrectionEntity> subRoot = subquery.from(CorrectionEntity.class);
      subquery.select(builder.greatest(subRoot.get(created))).where(builder.equal(subRoot.get(outlierId), outlier.get(OutlierEntity_.id)));

      final var decisionsPredicates = outlierSearch.getJobStatuses().stream()
        .map(outlierClass -> builder.equal(correctionJoin.get(CorrectionEntity_.outlierClass), outlierClass.getNumberRepresentation()))
        .toList();

      predicates.add(builder.equal(correctionJoin.get(created), subquery));
      predicates.add(builder.or(decisionsPredicates.toArray(new Predicate[0])));
    }

    if (outlierSearch.getStatus() != null) {
      predicates.add(builder.equal(outlier.get(status), outlierSearch.getStatus()));
    }

    if (outlierSearch.getSearch() != null) {
      String searchTerm = outlierSearch.getSearch().toLowerCase();
      List<Predicate> searchList = new ArrayList<>();

      searchList.add(builder.like(builder.lower(outlier.get(companyName)), "%" + searchTerm + "%"));
      searchList.add(builder.like(builder.lower(outlier.get(modelText)), "%" + searchTerm + "%"));

      boolean isNumeric = searchTerm.chars().allMatch(Character::isDigit);
      if (isNumeric) {
        searchList.add(builder.equal(outlier.get(objectId), Long.valueOf(searchTerm)));
        searchList.add(builder.equal(outlier.get(gsnr), Integer.valueOf(searchTerm)));
      }

      predicates.add(builder.or(searchList.toArray(new Predicate[0])));
    }
    return predicates;
  }

  @Nonnull
  private List<Order> getOrders(final Pageable pageable, final Root<OutlierEntity> outlier) {
    Sort sort = pageable.getSort();
    List<Order> orders = new ArrayList<>();

    sort.forEach(sortOrder -> {
      OutlierType property = OutlierType.valueOf(sortOrder.getProperty());
      Path<Double> propertyPath = switch (property) {
        case SALES_VOLUME -> outlier.get(salesVolume);
        case PRICE -> outlier.get(priceOrg);
        case TURNOVER -> outlier.get(turnover);
      };
      Order order = sortOrder.isAscending() ? builder.asc(propertyPath) : builder.desc(propertyPath);
      orders.add(order);
    });
    return orders;
  }
}
