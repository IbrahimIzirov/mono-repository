package com.local.strategies.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import java.io.Serial;
import java.io.Serializable;

@Setter(AccessLevel.PACKAGE)
@Getter(AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED, staticName = "of")
@Entity
@Table(name = "excluded_product_groups")
@EqualsAndHashCode
public class ExcludedProductGroupEntity implements Serializable {

  @Serial
  private static final long serialVersionUID = -283502601627187677L;

  @Id
  @Column(name = "exclusion_id", nullable = false)
  private long exclusionId;

  @Id
  @JoinColumn(name = "product_group_id", nullable = false)
  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  private ProductGroupEntity productGroup;
}
