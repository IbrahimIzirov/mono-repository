package com.local.strategies.models;

import com.local.contract.JobStatus;
import com.local.contract.OutlierCorrectionType;
import com.local.contract.OutlierStatus;
import com.local.contract.OutlierType;
import com.local.contract.page.Paging;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import jakarta.annotation.Nullable;
import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OutlierFilter extends Paging {
  private int countryId;
  private int productGroupId;
  private String period;
  private @Nullable OutlierType type;
  private @Nullable OutlierCorrectionType decidedBy;
  private @Nullable Collection<JobStatus> jobStatuses;
  private @Nullable OutlierStatus status;
  private @Nullable String search;
  private @Nullable OutlierType sort;

  public int getCountryId() {
    return countryId;
  }

  public void setCountryId(final int countryId) {
    this.countryId = countryId;
  }

  public int getProductGroupId() {
    return productGroupId;
  }

  public void setProductGroupId(final int productGroupId) {
    this.productGroupId = productGroupId;
  }

  public String getPeriod() {
    return period;
  }

  public void setPeriod(final String period) {
    this.period = period;
  }

  @Nullable
  public OutlierType getType() {
    return type;
  }

  public void setType(@Nullable final OutlierType type) {
    this.type = type;
  }

  @Nullable
  public OutlierCorrectionType getDecidedBy() {
    return decidedBy;
  }

  public void setDecidedBy(@Nullable final OutlierCorrectionType decidedBy) {
    this.decidedBy = decidedBy;
  }

  @Nullable
  public Collection<JobStatus> getJobStatuses() {
    return jobStatuses;
  }

  public void setJobStatuses(@Nullable final Collection<JobStatus> jobStatuses) {
    this.jobStatuses = jobStatuses;
  }

  @Nullable
  public OutlierStatus getStatus() {
    return status;
  }

  public void setStatus(@Nullable final OutlierStatus status) {
    this.status = status;
  }

  @Nullable
  public String getSearch() {
    return search;
  }

  public void setSearch(@Nullable final String search) {
    this.search = search;
  }

  @Nullable
  public OutlierType getSort() {
    return sort;
  }

  public void setSort(@Nullable final OutlierType sort) {
    this.sort = sort;
  }
}
