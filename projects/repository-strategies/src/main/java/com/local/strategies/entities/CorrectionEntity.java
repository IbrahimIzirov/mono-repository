package com.local.strategies.entities;

import com.local.contract.OutlierCorrectionType;

import jakarta.persistence.EnumType;

import jakarta.persistence.Enumerated;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import org.hibernate.annotations.JdbcType;
import org.hibernate.dialect.PostgreSQLEnumJdbcType;

import java.io.Serial;
import java.io.Serializable;
import java.time.ZonedDateTime;

@Getter
@Setter(AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "corrections")
public class CorrectionEntity implements Serializable {

  @Serial
  private static final long serialVersionUID = -7284464616423675975L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "outlier_id", nullable = false)
  private Long outlierId;

  @Column(name = "product_group_id", nullable = false)
  private int productGroupId;

  @Column(name = "outlier_class", nullable = false)
  private int outlierClass;

  @Column(name = "corrected_sales_volume")
  private Double correctedSalesVolume;

  @Column(name = "corrected_price")
  private Double correctedPrice;

  @Column(name = "corrected_turnover")
  private Double correctedTurnover;

  @Enumerated(EnumType.STRING)
  @Column(name = "correction_type", nullable = false)
  @JdbcType(PostgreSQLEnumJdbcType.class)
  private OutlierCorrectionType correctionType;

  @Column(name = "correction_reason")
  private String correctionReason;

  @Column(name = "created", nullable = false)
  private ZonedDateTime created;
}
