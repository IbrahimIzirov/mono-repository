package com.local.strategies.entities;

import com.local.contract.ConfigurationType;
import com.local.contract.OutlierStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;
import org.hibernate.annotations.JdbcType;
import org.hibernate.annotations.PartitionKey;
import org.hibernate.dialect.PostgreSQLEnumJdbcType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.io.Serial;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Objects;

import static jakarta.persistence.CascadeType.ALL;
import static jakarta.persistence.CascadeType.MERGE;
import static jakarta.persistence.EnumType.STRING;

@Getter
@Setter(AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PACKAGE, staticName = "of")
@Entity
@Table(name = "outliers")
public class OutlierEntity implements Serializable {

  @Serial
  private static final long serialVersionUID = -8798013577783945813L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "delivery_type_id", nullable = false)
  private int deliveryTypeId;

  @Column(name = "delivery_sequence", nullable = false)
  private int deliverySequence;

  @Column(name = "shop_id", nullable = false)
  private long shopId;

  @Column(name = "object_id", nullable = false)
  private long objectId;

  @PartitionKey
  @ManyToOne(fetch = FetchType.EAGER, cascade = MERGE)
  @JoinColumn(name = "product_group_id", nullable = false)
  private ProductGroupEntity productGroup;

  @ManyToOne(fetch = FetchType.EAGER, cascade = MERGE)
  @JoinColumn(name = "period_id", nullable = false)
  private PeriodEntity period;

  @Column(name = "sales_volume", nullable = false)
  private double salesVolume;

  @Column(name = "price_org", nullable = false)
  private double priceOrg;

  @Column(name = "price_calc", nullable = false)
  private double priceCalc;

  @Column(name = "turnover", nullable = false)
  private double turnover;

  @Column(name = "outlier_by_sales_volume", nullable = false)
  private boolean outlierBySalesVolume;

  @Column(name = "outlier_by_price", nullable = false)
  private boolean outlierByPrice;

  @Column(name = "outlier_by_turnover", nullable = false)
  private boolean outlierByTurnover;

  @Column(name = "probability_coding_matching")
  private Double probabilityCodingMatching;

  @Column(name = "probability_promotion")
  private Double probabilityPromotion;

  @Column(name = "probability_syntactic")
  private Double probabilitySyntactic;

  @Column(name = "probability_semantic")
  private Double probabilitySemantic;

  @Column(name = "probability_no_Error")
  private Double probabilityNoError;

  @Column(name = "predicted_sales_Volume")
  private Double predictedSalesVolume;

  @Column(name = "predicted_price")
  private Double predictedPrice;

  @Column(name = "predicted_turnover")
  private Double predictedTurnover;

  @Column(name = "auto_correction", nullable = false)
  private boolean autoCorrection;

  @JoinColumn(name = "outlier_id")
  @OneToMany(cascade = ALL, fetch = FetchType.EAGER)
  private Collection<CorrectionEntity> corrections;

  @Column(name = "dashboard_plots", nullable = false)
  private String dashboardPlots;

  @Column(name = "dashboard_master", nullable = false)
  private String dashboardMaster;

  @Column(name = "created", nullable = false)
  private ZonedDateTime created;

  @Column(name = "brand_text")
  private String brandText;

  @Column(name = "company_id", nullable = false)
  private int companyId;

  @Column(name = "company_name")
  private String companyName;

  @Column(name = "channel_long_text")
  private String channelLongText;

  @Column(name = "gsnr")
  private Integer gsnr;

  @Column(name = "model_text")
  private String modelText;

  @Column(name = "status", nullable = false)
  @Enumerated(STRING)
  private OutlierStatus status;

  @Column(name = "iav_main_text")
  private String iavMainText;

  @Column(name = "iav_other_text")
  private String iavOtherText;

  @Column(name = "iav_manufacturer")
  private String iavManufacturer;

  @Column(name = "iav_translation_code_type")
  private String iavTranslationCodeType;

  @Column(name = "iav_translation_code")
  private String iavTranslationCode;

  @Column(name = "mdm_item_model_text")
  private String mdmItemModelText;

  @Column(name = "mdm_item_feature_text")
  private String mdmItemFeatureText;

  @Column(name = "mdm_detail_model_text")
  private String mdmDetailModelText;

  @Column(name = "mdm_detail_feature_text")
  private String mdmDetailFeatureText;

  @Column(name = "rcm_feature_data")
  private String rcmFeatureData;

  @Column(name = "configuration", nullable = false)
  @Enumerated(EnumType.STRING)
  @JdbcType(PostgreSQLEnumJdbcType.class)
  private ConfigurationType configuration;

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    final OutlierEntity that = (OutlierEntity) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
