package com.local.strategies.jpa;

import com.local.contract.Company;
import com.local.strategies.entities.ExclusionEntity;
import com.local.strategies.models.OutlierClassClassifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
public interface NonEntityJpaRepository extends JpaRepository<ExclusionEntity, Long> {

  // Will return Company class from database, which is not Entity
  @Query(value = """
        SELECT new com.local.strategies.Company(e.companyId, e.companyName)
        FROM ExclusionEntity e
        WHERE e.country.id = :countryId AND LOWER(e.companyName) LIKE LOWER(CONCAT('%', :namePart, '%'))
    """)
  List<Company> findCompaniesByCountryAndNameContains(@Param("countryId") int countryId,
                                                      @Param("namePart") String namePart);

  @Query(value = """
        SELECT new com.local.strategies.OutlierClassClassifier(MAX(o.period.periodId), c.outlierClass, CAST(COUNT(c) as integer))
        FROM OutlierEntity o
        JOIN o.corrections c
        WHERE o.productGroup.id = :productGroupId AND o.period.periodicity = :periodicity AND c.correctionType = 'USER' AND c.created > :createdAfter
        GROUP BY c.outlierClass
    """)
  List<OutlierClassClassifier> findClassClassifiers(@Param("productGroupId") int productGroupId,
                                                    @Param("periodicity") String periodicity,
                                                    @Param("createdAfter") ZonedDateTime createdAfter);
}
