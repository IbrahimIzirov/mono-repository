package com.local.strategies.entities;

import io.hypersistence.utils.hibernate.type.array.ListArrayType;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.io.Serial;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

import static jakarta.persistence.CascadeType.MERGE;

@Setter(AccessLevel.PACKAGE)
@Getter(AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "exclusions")
@EqualsAndHashCode
public class ExclusionEntity implements Serializable {

  @Serial
  private static final long serialVersionUID = 1399561946292476044L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private long id;

  @ManyToOne(fetch = FetchType.EAGER, cascade = MERGE)
  @JoinColumn(name = "country_id", nullable = false)
  private CountryEntity country;

  @Column(name = "reason")
  private String reason;

  @Column(name = "company_id", nullable = false)
  private int companyId;

  @Column(name = "company_name", nullable = false)
  private String companyName;

  @CreationTimestamp
  @Column(name = "created")
  private ZonedDateTime created;

  @Type(ListArrayType.class)
  @Column(name = "shops", nullable = false)
  private List<Long> shops;

  @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "exclusion_id", referencedColumnName = "id")
  private List<ExcludedProductGroupEntity> excludedProductGroups;

  ExclusionEntity(int companyId, String companyName, CountryEntity country, String reason, List<Long> shops) {
    this.companyName = companyName;
    this.companyId = companyId;
    this.country = country;
    this.reason = reason;
    this.shops = shops;
  }


  static ExclusionEntity of(int companyId, String companyName, CountryEntity country, String reason, List<Long> shops) {
    return new ExclusionEntity(companyId, companyName, country, reason, shops);
  }
}
