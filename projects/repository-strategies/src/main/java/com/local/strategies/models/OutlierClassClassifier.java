package com.local.strategies.models;

public record OutlierClassClassifier(
  int latestPeriodId,
  int classifier,
  int totalCount
) {
}
