package com.local.strategies.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Setter(AccessLevel.PACKAGE)
@Getter(AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED, staticName = "of")
@Entity
@Table(name = "excluded_product_groups")
@EqualsAndHashCode
public class ProductGroupEntity implements Serializable {

  @Serial
  private static final long serialVersionUID = 659970292407198010L;

  @Id
  @Column(name = "id", nullable = false)
  private long id;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "country_id", nullable = false)
  private CountryEntity country;

  @CreationTimestamp
  @Column(name = "created", nullable = false)
  private LocalDateTime created;
}
