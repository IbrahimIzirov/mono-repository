package com.local.strategies.jpa;

import com.local.strategies.entities.ExclusionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface NormalJpaRepository extends JpaRepository<ExclusionEntity, Long> {

  @Transactional
  @Modifying
  @Query(nativeQuery = true, value = """
    DELETE FROM exclusions WHERE id = :id
    """)
  void deleteExclusion(@Param("id") long id);
}
