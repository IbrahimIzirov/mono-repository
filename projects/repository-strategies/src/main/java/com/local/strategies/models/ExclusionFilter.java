package com.local.strategies.models;

import com.local.contract.page.Paging;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import jakarta.annotation.Nullable;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ExclusionFilter extends Paging {
  private Integer countryId;
  private @Nullable String searchParam;

  public Integer getCountryId() {
    return countryId;
  }

  void setCountryId(final Integer countryId) {
    this.countryId = countryId;
  }

  @Nullable
  public String getSearchParam() {
    return searchParam;
  }

  void setSearchParam(@Nullable final String searchParam) {
    this.searchParam = searchParam;
  }
}
