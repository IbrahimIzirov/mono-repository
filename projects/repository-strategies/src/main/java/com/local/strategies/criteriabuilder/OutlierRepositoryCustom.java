package com.local.strategies.criteriabuilder;

import com.local.strategies.entities.OutlierEntity;
import com.local.strategies.models.OutlierFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OutlierRepositoryCustom {

  Page<OutlierEntity> findOutliers(Pageable pageable, OutlierFilter filter);
}
