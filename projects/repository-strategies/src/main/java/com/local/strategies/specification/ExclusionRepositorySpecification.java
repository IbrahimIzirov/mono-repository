package com.local.strategies.specification;

import com.local.strategies.entities.CountryEntity_;
import com.local.strategies.entities.ExclusionEntity;
import com.local.strategies.models.ExclusionFilter;
import org.springframework.data.jpa.domain.Specification;

import jakarta.annotation.Nonnull;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import static com.local.strategies.entities.ExclusionEntity_.companyId;
import static com.local.strategies.entities.ExclusionEntity_.companyName;
import static com.local.strategies.entities.ExclusionEntity_.country;
import static com.local.strategies.entities.ExclusionEntity_.shops;

public class ExclusionRepositorySpecification implements Specification<ExclusionEntity> {

  @Serial
  private static final long serialVersionUID = -1745721688528660878L;

  private final ExclusionFilter filter;

  ExclusionRepositorySpecification(final ExclusionFilter filter) {
    this.filter = filter;
  }

  @Override
  public Predicate toPredicate(final Root<ExclusionEntity> root, @Nonnull final CriteriaQuery<?> query, @Nonnull final CriteriaBuilder criteriaBuilder) {
    List<Predicate> predicates = new ArrayList<>();
    predicates.add(criteriaBuilder.equal(root.get(country).get(CountryEntity_.id), filter.getCountryId()));

    if (filter.getSearchParam() != null) {
      String searchTerm = filter.getSearchParam().toLowerCase();
      List<Predicate> searchList = new ArrayList<>();
      searchList.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(companyName)), "%" + searchTerm + "%"));

      boolean isNumeric = searchTerm.chars().allMatch(Character::isDigit);
      if (isNumeric) {
        long numericValue = Long.parseLong(searchTerm);

        // We are doing check because companyId is int everywhere in our system, and if we provide long - will cause exception.
        if (numericValue <= Integer.MAX_VALUE) {
          searchList.add(criteriaBuilder.equal(root.get(companyId), numericValue));
        }

        searchList.add(criteriaBuilder.isTrue(
          criteriaBuilder.equal(criteriaBuilder.function("sql", Boolean.class,
            criteriaBuilder.literal("? = ANY(?)"), criteriaBuilder.literal(numericValue), root.get(shops)), criteriaBuilder.literal(Boolean.TRUE))
        ));
      }

      // In case if we need to add orders to our queries.
//      query.orderBy(getOrders(criteriaBuilder, root));

      predicates.add(criteriaBuilder.or(searchList.toArray(new Predicate[0])));
    }

    return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
  }

//  @Nonnull
//  private List<Order> getOrders(final CriteriaBuilder builder,
//                                final Root<ExclusionEntity> root) {
//    List<Order> orders = new ArrayList<>();
//    orders.add(builder.asc(root.get(companyId)));
//    return orders;
//  }
}
