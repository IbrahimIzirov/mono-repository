plugins {
  id("spring-boot-dependencies")
  id("jpa-static-conventions")
}

group = "com.local"

dependencies {
  implementation(project(":model-contracts"))
  implementation("org.hibernate.orm:hibernate-core:6.3.1.Final")
  implementation("org.springframework.boot:spring-boot-starter-actuator")
  implementation("org.springframework.boot:spring-boot-starter-validation")
  implementation("org.springframework.boot:spring-boot-starter-data-jpa")
  implementation("org.springframework.boot:spring-boot-starter-web")
  implementation("io.hypersistence:hypersistence-utils-hibernate-63")
  runtimeOnly("org.postgresql:postgresql")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("org.assertj:assertj-core")
}
