plugins {
  id("spring-boot-conventions")
}

group = "com.local"

dependencies {
  implementation("org.webjars:swagger-ui:5.17.2")
  implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.2.0")
  implementation("org.springdoc:springdoc-openapi-starter-webflux-ui:2.2.0")
  compileOnly("org.springframework.boot:spring-boot-starter-web")
  compileOnly("org.springframework.boot:spring-boot-starter-webflux")
}
