package com.local.swagger;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.net.URI;

import static org.springframework.http.HttpStatus.FOUND;

@Controller
abstract sealed class BaseConfiguration permits WebMvcConfiguration, WebfluxConfiguration {
  protected static final String WEBJAR_LOCATION = "/META-INF/resources/webjars/swagger-ui/5.17.2/";

  // only use if you have openapi-yaml file
  // url: "/openapi.yaml",
  protected static final String SWAGGER_INITIALIZER_JS = """
    window.onload = function() {
      window.ui = SwaggerUIBundle({
        url: "/v3/api-docs",
        dom_id: '#swagger-ui',
        deepLinking: true,
        presets: [
          SwaggerUIBundle.presets.apis,
          SwaggerUIStandalonePreset
        ],
        plugins: [
          SwaggerUIBundle.plugins.DownloadUrl
        ],
        layout: "StandaloneLayout"
      });
    };
    """;

  @GetMapping(value = "/", produces = MediaType.TEXT_PLAIN_VALUE)
  ResponseEntity<Void> index() {
    return ResponseEntity.status(FOUND)
      .location(URI.create("swagger-ui/index.html"))
      .build();
  }

  // Remove this bean if you have openApi.yaml file.
  @Bean
  public OpenAPI customOpenAPI() {
    return new OpenAPI()
      .info(new Info()
        .title("My API")
        .version("1.0")
        .description("API documentation for my application"));
  }

  // Configurations for OpenAPI file existing.
//  @Value("classpath:/openapi.yaml")
//  private Resource openapi;
//
//  @GetMapping(value = "/openapi.yaml", produces = "application/vnd.oai.openapi")
//  @ResponseBody
//  String openapiYaml() throws IOException {
//    return openapiContent();
//  }
//
//  private String openapiContent() throws IOException {
//    try (InputStream is = openapi.getInputStream()) {
//      return StreamUtils.copyToString(is, Charset.defaultCharset());
//    }
//  }
}
