package com.local.swagger;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.CacheControl;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.ResourceTransformer;
import org.springframework.web.servlet.resource.TransformedResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
non-sealed class WebMvcConfiguration extends BaseConfiguration {

  private static ResourceTransformer resourceTransformer() {
    return (exchange, resource, transformerChain) -> {
      try {
        final String url = resource.getURL().toString();
        if (new AntPathMatcher().match("**/swagger-ui/**/swagger-initializer.js", url)) {
          return new TransformedResource(resource, SWAGGER_INITIALIZER_JS.getBytes(StandardCharsets.UTF_8));
        }
      } catch (IOException ignored) {
      }
      return resource;
    };
  }

  @Bean
  WebMvcConfigurer webMvcConfigurer() {
    return new WebMvcConfigurer() {
      @Override
      public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/swagger-ui/**")
          .addResourceLocations("classpath:" + WEBJAR_LOCATION)
          .setCacheControl(CacheControl.noCache())
          .resourceChain(false)
          .addTransformer(resourceTransformer());

        registry.addResourceHandler("/swagger-ui/*")
          .addResourceLocations("classpath:" + WEBJAR_LOCATION)
          .resourceChain(false)
          .addTransformer(resourceTransformer());
      }
    };
  }
}
