package com.local.swagger;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * If you want to activate swagger on some module it's need just to add lib in gradle file.
 * And annotation @EnableSwaggerUI in configuration files.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({WebfluxConfiguration.class, WebMvcConfiguration.class})
public @interface EnableSwaggerUI {
}
