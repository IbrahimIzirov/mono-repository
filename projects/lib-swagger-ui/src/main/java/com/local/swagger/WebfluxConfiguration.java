package com.local.swagger;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.CacheControl;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.reactive.config.ResourceHandlerRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.resource.ResourceTransformer;
import org.springframework.web.reactive.resource.TransformedResource;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
non-sealed class WebfluxConfiguration extends BaseConfiguration {

  @Bean
  WebFluxConfigurer webFluxConfigurer() {
    return new WebFluxConfigurer() {
      @Override
      public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/swagger-ui/**")
          .addResourceLocations("classpath:" + WEBJAR_LOCATION)
          .setCacheControl(CacheControl.noCache())
          .resourceChain(false)
          .addTransformer(resourceTransformer());

        registry.addResourceHandler("/swagger-ui/*")
          .addResourceLocations("classpath:" + WEBJAR_LOCATION)
          .resourceChain(false)
          .addTransformer(resourceTransformer());
      }
    };
  }

  private ResourceTransformer resourceTransformer() {
    return (exchange, resource, transformerChain) -> {
      try {
        final String url = resource.getURL().toString();
        if (new AntPathMatcher().match("**/swagger-ui/**/swagger-initializer.js", url)) {
          return Mono.just(new TransformedResource(resource, SWAGGER_INITIALIZER_JS.getBytes(StandardCharsets.UTF_8)));
        }
      } catch (IOException ignored) {
      }
      return Mono.just(resource);
    };
  }
}
