plugins {
  id("spring-boot-conventions")
}

group = "com.local"

dependencies {
  implementation("org.springframework:spring-context")
  implementation("io.micrometer:micrometer-core")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
}
