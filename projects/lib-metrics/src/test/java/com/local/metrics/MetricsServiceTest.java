package com.local.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringJUnitConfig
@ExtendWith(MockitoExtension.class)
@Import(MetricsServiceTest.SpringTestConfig.class)
@TestPropertySource(properties = {
  "spring.application.name=test_app",
  "spring.application.instance_id=1"
})
class MetricsServiceTest {

  @Autowired
  MeterRegistry meterRegistry;

  @Autowired
  MetricsFactory metricsFactory;

  Metric metricsCounter;
  Metric metricsTimer;
  CounterService counterService;
  TimerService timerService;
  MetricsMeta metricsMeta;

  @BeforeEach
  void beforeEach() {
    metricsCounter = Metric.of(randomString(), randomString());
    metricsTimer = Metric.of(randomString(), randomString());
    counterService = metricsFactory.createCounter(metricsCounter);
    timerService = metricsFactory.createTimer(metricsTimer);
    metricsMeta = new MetricsMeta(18218, 9515);
  }

  @Test
  void testCounter() {
    counterService.count(metricsMeta, 2);

    final Optional<Counter> numberOfRecords = Optional.of(meterRegistry
      .find(metricsCounter.metricName()).tags(metricsMeta.plain()).counter());
    assertTrue(numberOfRecords.isPresent());

    assertEquals(2, numberOfRecords.get().count());

    counterService.count(metricsMeta, 100L);
    assertEquals(102, numberOfRecords.get().count());
  }

  @Test
  void testTimer() {
    Duration duration = Duration.ofSeconds(5L);
    timerService.timer(metricsMeta, duration);

    final Optional<Timer> timeTaken = Optional.of(meterRegistry
      .find(metricsTimer.metricName()).tags(metricsMeta.plain()).timer());

    assertTrue(timeTaken.isPresent());

    assertEquals(5L, timeTaken.get().totalTime(TimeUnit.SECONDS));

    timerService.timer(metricsMeta, duration.plus(Duration.ofSeconds(5L)));
    assertEquals(15L, timeTaken.get().totalTime(TimeUnit.SECONDS));
  }

  @Test
  void testMeta() {
    counterService.count(metricsMeta, 1);

    final Optional<Counter> numberOfRecords = Optional.of(meterRegistry
      .find(metricsCounter.metricName()).tags(metricsMeta.plain()).counter());
    assertTrue(numberOfRecords.isPresent());

    final Counter counter = numberOfRecords.get();
    final List<Tag> counterTags = counter.getId().getTags();
    final Tags expectedTags = metricsMeta.plain(Tags::of);

    assertThat(counterTags).containsAll(expectedTags);
  }

  @TestConfiguration
  @Import({MetricsConfiguration.class})
  static class SpringTestConfig {

    @Bean
    MeterRegistry meterRegistry() {
      final SimpleMeterRegistry meterRegistry = new SimpleMeterRegistry();
      Metrics.addRegistry(meterRegistry);
      return meterRegistry;
    }
  }

  private static String randomString() {
    return Integer.toHexString(LocalDateTime.now().hashCode());
  }
}
