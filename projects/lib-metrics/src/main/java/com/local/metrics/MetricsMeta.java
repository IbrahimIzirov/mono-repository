package com.local.metrics;

import lombok.Getter;
import lombok.Setter;

import java.util.function.Function;

@Getter
@Setter
public class MetricsMeta {
  private final long calculationParameter;
  private final long definitionParameter;

  public MetricsMeta(final long calculationParameter, final long definitionParameter) {
    this.calculationParameter = calculationParameter;
    this.definitionParameter = definitionParameter;
  }

  public String[] plain() {
    return new String[]{
      "calculation_parameter", String.valueOf(calculationParameter),
      "definition_parameter", String.valueOf(definitionParameter)
    };
  }

  public <T> T plain(final Function<String[], T> transformer) {
    return transformer.apply(plain());
  }
}
