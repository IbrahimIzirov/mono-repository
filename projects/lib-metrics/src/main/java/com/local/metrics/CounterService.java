package com.local.metrics;

public interface CounterService {
  void count(MetricsMeta metricsMeta, long number);
}
