package com.local.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

public class MetricsConfiguration {

  @Value("${spring.application.name}")
  private String serviceName;

  @Value("${spring.application.instance_id:${random.value}}")
  private String serviceInstanceId;

  @Bean
  MetricsFactory metricsFactory(final MeterRegistry meterRegistry) {
    return new MetricsFactory() {
      final Tags tags = Tags.of("service_name", serviceName, "service_instance_id", serviceInstanceId);

      @Override
      public CounterService createCounter(final Metric metric) {
        return (metricsMeta, number) -> {
          final var internalCounter = Counter.builder(metric.metricName())
            .description(metric.metricDescription())
            .tags(Tags.concat(tags, metricsMeta.plain()))
            .register(meterRegistry);
          internalCounter.increment(number);
        };
      }

      @Override
      public TimerService createTimer(final Metric metric) {
        return (metricsMeta, duration) -> {
          final var internalTimer = Timer.builder(metric.metricName())
            .description(metric.metricDescription())
            .tags(Tags.concat(tags, metricsMeta.plain()))
            .register(meterRegistry);
          internalTimer.record(duration);
        };
      }
    };
  }
}
