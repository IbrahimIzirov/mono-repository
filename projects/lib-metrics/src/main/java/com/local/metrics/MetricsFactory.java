package com.local.metrics;

public interface MetricsFactory {

  CounterService createCounter(Metric metric);

  TimerService createTimer(Metric metric);
}
