package com.local.metrics;

import java.time.Duration;

public interface TimerService {
  void timer(MetricsMeta metricsMeta, Duration duration);
}
