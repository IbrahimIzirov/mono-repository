package com.local.metrics;

public interface Metric {
  String metricName();

  String metricDescription();

  static Metric of(final String name, final String description) {
    return new Metric() {
      @Override
      public String metricName() {
        return name;
      }

      @Override
      public String metricDescription() {
        return description;
      }
    };
  }
}
